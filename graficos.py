#!/usr/bin/python

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np


def graficoSpeedup(y2, y4, y8, y16, ejercicio):
    n_values = np.array([512, 1024, 2048, 4096])
    plt.plot(n_values.astype("str"), y2, linestyle="-", marker="o", label="2")
    plt.plot(n_values.astype("str"), y4, linestyle="-", marker="o", label="4")
    plt.plot(n_values.astype("str"), y8, linestyle="-", marker="o", label="8")
    plt.plot(n_values.astype("str"), y16, linestyle="-", marker="o", label="16")

    # yaxis number scale
    plt.gca().yaxis.set_major_locator(MultipleLocator(base=2.0))

    plt.ylabel("Speedup")
    plt.xlabel("N")
    plt.title("Speedup con la solucion de %s" % ejercicio)

    # show the legend below the plot
    plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.15), ncol=4)

    # make horizontal transparent lines to help with reading the y axis
    plt.grid(axis="y")

    # make yaxis values color grey instead of black
    plt.gca().tick_params(axis="y", colors="grey")

    # remove plot borders
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)
    plt.gca().spines["left"].set_visible(False)

    plt.savefig(
        "images/graficoSpeedup%s.png" % ejercicio,
        bbox_inches="tight",
        dpi=600,
    )
    plt.show()


def graficoEficiencia(eficiencia, ejercicio):
    n_values = np.array([512, 1024, 2048, 4096])
    x = np.arange(len(n_values))
    width = 0.22
    multiplier = 0
    fig, ax = plt.subplots(layout="constrained")

    for attribute, measurement in eficiencia.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, measurement, width, label=attribute)
        ax.bar_label(rects, padding=3)
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel("Eficiencia")
    ax.set_xlabel("N")
    ax.set_title("Eficiencia con la solucion de %s" % ejercicio)
    ax.set_xticks(x + width, n_values.astype("str"))
    ax.set_ylim(0, 1)

    # show the legend below the plot
    plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.15), ncol=4)

    # make horizontal lines to help reading the y axis, make values appear in front of the lines
    plt.grid(axis="y", zorder=0)
    # yaxis values zorder
    plt.gca().set_axisbelow(True)

    # make yaxis values color grey instead of black
    plt.gca().tick_params(axis="y", colors="grey")

    # remove plot borders
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)
    plt.gca().spines["left"].set_visible(False)

    plt.savefig(
        "images/graficoEficiencia%s.png" % ejercicio,
        bbox_inches="tight",
        dpi=600,
    )
    plt.show()


graficoSpeedup(
   [1,2,3,4],
   [4,5,6,7],
   [7,8,9,10],
   [10,11,12,13],
   "ejercicio1"
)

graficoEficiencia(
    {
        "2": [0.8, 0.7, 0.5, 0.3],
        "4": [0.7, 0.6, 0.7, 0.8],
        "8": [0.5, 0.6, 0.8, 0.9],
    },
    "ejercicio1",
)
