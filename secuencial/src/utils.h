#ifndef UTILS_H
#define UTILS_H

// generador de numeros aleatorios entre min y max
#define randnum(min, max) ((rand() % (int)(((max) + 1) - (min))) + (min))

/**
** Retorna el tiempo actual en segundos
**/
double dwalltime();

/**
** Aloca memoria para un tamaño de bloque especificado
** bs: tamaño del bloque
**/
double *alloc_double(int bs);

/**
** Inicializa un vector de tamaño N con valor -1
** v: vector de tamaño N, con tipo de dato int
**/
void init_vector(int *v, int N);

/**
** Inicializa una matriz de tamaño NxN segun el valor especificado
** m: matriz de tamaño NxN, con tipo de dato int
** value: como inicializar, si es 0, se inicializa en 0, si no random
** N: dimension de la matriz
**/
void init_matrix_int(int *m, int value, int N);

/**
** Inicializa una matriz de tamaño NxN segun el valor especificado
** m: matriz de tamaño NxN, con tipo de dato double
** value: como inicializar, si es 0, se inicializa en 0, si no random
** row: si es 0 esta ordenada por filas, si no esta ordenada por columnas
** N: dimension de la matriz
**/
void init_matrix_double(double *m, double value, int row, int N);

/**
** Imprime una matriz de tamaño NxN de tipo int
** m: matriz de tamaño NxN, con tipo de dato int
** N: dimension de la matriz
**/
void print_matrix_int(int *m, int N);

/**
** Imprime una matriz de tamaño NxN de tipo double
** m: matriz de tamaño NxN, con tipo de dato double
** row: si es 0 esta ordenada por filas, si no esta ordenada por columnas
** N: dimension de la matriz
**/
void print_matrix_double(double *m, int row, int N);

/**
** Imprime todas las matrices y vectores utilizadas en el programa a la hora de
** calcular la expresion matematica
**/
void print_all(double *A, double *B, double *C, double *R, double *temp, int *D,
               double *max_min_prom_A, double *max_min_prom_B, double cociente,
               int N);

#endif
