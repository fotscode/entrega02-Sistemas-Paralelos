#include "matrix_ops.h"
#include "utils.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void calcular_potencias(int *potencias, int N) {
  int register i;
  for (i = 1; i < N; i++) {
    potencias[i] = i * i;
  }
}

int main(int argc, char *argv[]) {
  // TODO: que utilizar con register y que no
  int register N, BS;

  // ∨∨∨∨ checkeo de parametros ∨∨∨∨
  if ((argc != 3) || ((N = atoi(argv[1])) <= 0) ||
      ((BS = atoi(argv[2])) <= 0) || ((N % BS) != 0)) {
    printf("Error en los parámetros. Usage: ./%s N BS (N debe ser multiplo "
           "de BS)\n",
           argv[0]);
    exit(1);
  }

#ifdef CANT /* si se compila con -D CANT=NUMERO */
  double promedio = 0;
  for (size_t i = 0; i < CANT; i++) {
#endif /* ifdef CANT */
    double *A, *B, *C, *R, *temp, cociente, init_tick, final_tick,
        *max_min_prom_A, *max_min_prom_B;
    int *D, *potencias;
    // ∨∨∨∨ fase inicializacion ∨∨∨∨
    max_min_prom_A = alloc_double(3);
    max_min_prom_B = alloc_double(3);
    A = alloc_double(N * N);
    B = alloc_double(N * N);
    C = alloc_double(N * N);
    R = alloc_double(N * N);
    temp = alloc_double(N * N);
    D = (int *)malloc(sizeof(int) * N * N);
    potencias = (int *)malloc(sizeof(int) * 41);
    max_min_prom_A[0] = DBL_MIN;
    max_min_prom_A[1] = DBL_MAX;
    max_min_prom_A[2] = 0;

    max_min_prom_B[0] = DBL_MIN;
    max_min_prom_B[1] = DBL_MAX;
    max_min_prom_B[2] = 0;
    init_matrix_double(A, 5, 0, N);
    init_matrix_double(B, 5, 1, N);
    init_matrix_double(C, 5, 0, N);
    init_matrix_double(temp, 0, 0, N);
    init_vector(potencias, 41);
    init_matrix_int(D, 2, N);
    // ∨∨∨∨ fase procesamiento ∨∨∨∨
    init_tick = dwalltime();
    calcular_potencias(potencias, 41);
    max_min_prom(A, max_min_prom_A, N);
    max_min_prom(B, max_min_prom_B, N);
    cociente = (max_min_prom_A[2] * max_min_prom_B[2] != 0)
                   ? (max_min_prom_A[0] * max_min_prom_B[0] -
                      max_min_prom_A[1] * max_min_prom_B[1]) /
                         (max_min_prom_A[2] * max_min_prom_B[2])
                   : 0;
    matmulblks_double(A, B, R, N, BS);
    matmul_scalar(R, cociente, N);
    Pot2(D, N, potencias);
    matmulblks_double_int(C, D, temp, N, BS);
    matadd_double(R, temp, N);
    final_tick = dwalltime();
    // ∨∨∨∨ fase imprimir y liberar ∨∨∨∨
    printf("Tiempo en segundos %f \n", final_tick - init_tick);
#ifdef DEBUG
    print_all(A, B, C, R, temp, D, max_min_prom_A, max_min_prom_B, cociente, N);
#endif
    free(A);
    free(B);
    free(C);
    free(R);
    free(D);
    free(temp);
    free(max_min_prom_A);
    free(max_min_prom_B);
    free(potencias);
#ifdef CANT /* end del for */
    promedio += (final_tick - init_tick) / CANT;
  }
  printf("N: %d | BS: %d | Promedio: %f\n", N, BS, promedio);
#endif /* ifdef CANT */
  return 0;
}
