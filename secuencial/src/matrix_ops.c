#include "matrix_ops.h"

void max_min_prom(double *matrix, double *res, int N) {
  int register i, j, fila;
  double register valMatrix, suma;
  suma = 0;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      valMatrix = matrix[fila + j];
      if (res[0] < valMatrix)
        res[0] = valMatrix;
      if (res[1] > valMatrix)
        res[1] = valMatrix;
      suma += valMatrix;
    }
  }
  res[2] = suma / (N * N);
}

void matmulblks_double(double *a, double *b, double *c, int N, int BS) {
  int register i, j, k, fila, col, res;
  for (i = 0; i < N; i += BS) {
    fila = i * N;
    for (j = 0; j < N; j += BS) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < N; k += BS) {
        blkmul_double(&a[fila + k], &b[col + k], &c[res], N, BS);
      }
    }
  }
}

void blkmul_double(double *ablk, double *bblk, double *cblk, int N, int BS) {
  int register i, j, k, fila, col, res;
  double register suma;
  for (i = 0; i < BS; i++) {
    fila = i * N;
    for (j = 0; j < BS; j++) {
      col = j * N;
      res = fila + j;
      suma = 0;
      for (k = 0; k < BS; k++) {
        suma += ablk[fila + k] * bblk[col + k];
      }
      cblk[res] += suma;
    }
  }
}

void matmulblks_double_int(double *a, int *b, double *c, int N, int BS) {
  int register i, j, k, fila, col, res;
  for (i = 0; i < N; i += BS) {
    fila = i * N;
    for (j = 0; j < N; j += BS) {
      col = j * N;
      res = fila + j;
      for (k = 0; k < N; k += BS) {
        blkmul_double_int(&a[fila + k], &b[col + k], &c[res], N, BS);
      }
    }
  }
}

void blkmul_double_int(double *ablk, int *bblk, double *cblk, int N, int BS) {
  int register i, j, k, fila, col, res;
  double register suma;
  for (i = 0; i < BS; i++) {
    fila = i * N;
    for (j = 0; j < BS; j++) {
      col = j * N;
      res = fila + j;
      suma = 0;
      for (k = 0; k < BS; k++) {
        suma += ablk[fila + k] * bblk[col + k];
      }
      cblk[res] += suma;
    }
  }
}

void matmul_scalar(double *R, double cociente, int N) {
  int register i, j, fila;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      R[fila + j] *= cociente;
    }
  }
}

void matadd_double(double *a, double *b, int N) {
  int register i, j, fila, indice;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      indice = fila + j;
      a[indice] += b[indice];
    }
  }
}

void Pot2(int *d, int N, int *potencias) {
  int register i, j, indice, fila;
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      indice = fila + j;
      d[indice] = potencias[d[indice]];
    }
  }
}
