Cada directorio tiene dentro un Makefile, y un directorio src donde estan los archivos fuente y headers.

- Si se compila usando `make` se ejecuta el codigo una sola vez.
- Si se compila usando `make prom` se ejecuta el codigo 10 veces y se saca un promedio.
- Si se compila usando `make debug` se ejecuta el codigo el codigo una sola vez pero mostrando el resultado de las matrices y estructuras intermedias.
