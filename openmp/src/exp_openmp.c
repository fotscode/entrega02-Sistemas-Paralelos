#include "matrix_ops.h"
#include "utils.h"
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
double suma, max, min;
int paso = 0;

// calcula las potencias de 2 hasta 40
void calcular_potencias(int *potencias, int N) {
  int register i;
  for (i = 2; i < N; i++) {
    potencias[i] = i * i;
  }
}

void max_min_prom(double *matrix, double *res, int N) {
  /** la funcion esta definida aca ya que max, min y suma son variables globales
   ** esta misma fue previamente declarada en matrix_ops.h
   **/
  int register i, j, fila;
  double register valMatrix;

  #pragma omp for schedule(static) private(i,j,fila,valMatrix) reduction(+:suma) reduction(max:max) reduction(min:min) 
  for (i = 0; i < N; i++) {
    fila = N * i;
    for (j = 0; j < N; j++) {
      valMatrix = matrix[fila + j];
      if (max < valMatrix)
        max = valMatrix;
      if (min > valMatrix)
        min = valMatrix;
      suma += valMatrix;
    }
  }
  #pragma omp single
  {
    res[0] = max;
    res[1] = min;
    res[2] = suma / (N * N);
    if (paso++ == 0){
      suma = 0;
      max = DBL_MIN;
      min = DBL_MAX;
    }
  }
}
int main(int argc, char *argv[]) {
  int register N, BS, T;

  // ∨∨∨∨ checkeo de parametros ∨∨∨∨
  if ((argc != 4) || ((N = atoi(argv[1])) <= 0) || ((T = atoi(argv[2])) <= 0) ||
      ((BS = atoi(argv[3])) <= 0) || (N % (T * BS) != 0)) {
    printf("Error en los parámetros. Usage: %s N T BS\n", argv[0]);
    printf("N%%(T*BS) debe ser 0\n");
    exit(1);
  }

  omp_set_num_threads(T);
#ifdef CANT /* si se compila con -D CANT=NUMERO */
  double promedio = 0;
  for (size_t i = 0; i < CANT; i++) {
#endif /* ifdef CANT */
    double *A, *B, *C, *R, *temp, cociente, init_tick, final_tick,
        *max_min_prom_A, *max_min_prom_B;
    int *D, *potencias;
    // ∨∨∨∨ fase inicializacion ∨∨∨∨
    max_min_prom_A = alloc_double(3);
    max_min_prom_B = alloc_double(3);
    A = alloc_double(N * N);
    B = alloc_double(N * N);
    C = alloc_double(N * N);
    R = alloc_double(N * N);
    temp = alloc_double(N * N);
    D = (int *)malloc(sizeof(int) * N * N);
    potencias = (int *)malloc(sizeof(int) * 41);
    max_min_prom_A[0] = DBL_MIN;
    max_min_prom_A[1] = DBL_MAX;
    max_min_prom_A[2] = 0;

    max_min_prom_B[0] = DBL_MIN;
    max_min_prom_B[1] = DBL_MAX;
    max_min_prom_B[2] = 0;
    init_matrix_double(A, 5, 0, N);
    init_matrix_double(B, 5, 1, N);
    init_matrix_double(C, 5, 0, N);
    init_matrix_double(temp, 0, 0, N);
    init_vector(potencias, 41);
    init_matrix_int(D, 2, N);
    suma = 0;
    max = DBL_MIN;
    min = DBL_MAX;
    // ∨∨∨∨ fase procesamiento ∨∨∨∨
    init_tick = dwalltime();
    #pragma omp parallel 
    {
      #pragma omp single
      { 
        calcular_potencias(potencias, 41); 
      }
      max_min_prom(A, max_min_prom_A, N);
      max_min_prom(B, max_min_prom_B, N);
      cociente = (max_min_prom_A[2] * max_min_prom_B[2] != 0)
                     ? (max_min_prom_A[0] * max_min_prom_B[0] -
                        max_min_prom_A[1] * max_min_prom_B[1]) /
                           (max_min_prom_A[2] * max_min_prom_B[2])
                     : 0;
      matmulblks_double(A, B, R, N, BS);
      matmul_scalar(R, cociente, N);
      Pot2(D, N, potencias);
      matmulblks_double_int(C, D, temp, N, BS);
      matadd_double(R, temp, N);
    }
    final_tick = dwalltime();
    // ∨∨∨∨ fase imprimir y liberar ∨∨∨∨
    printf("Tiempo en segundos %f \n", final_tick - init_tick);
#ifdef DEBUG
    print_all(A, B, C, R, temp, D, max_min_prom_A, max_min_prom_B, cociente, N);
#endif
    free(A);
    free(B);
    free(C);
    free(R);
    free(D);
    free(temp);
    free(max_min_prom_A);
    free(max_min_prom_B);
    free(potencias);
#ifdef CANT /* end del for */
    promedio += (final_tick - init_tick) / CANT;
  }
  printf("N: %d | BS: %d | Promedio: %f\n", N, BS, promedio);
#endif /* ifdef CANT */
  return 0;
}
