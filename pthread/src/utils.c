#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

double dwalltime() {
  double sec;
  struct timeval tv;

  gettimeofday(&tv, NULL);
  sec = tv.tv_sec + tv.tv_usec / 1000000.0;
  return sec;
}

double *alloc_double(int bs) { return malloc(sizeof(double) * bs); }

void init_vector(int *v, int N) {
  for (int i = 0; i < N; i++) {
    v[i] = -1;
  }
}

void init_matrix_int(int *m, int value, int N) {
  int i, j;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      m[j * N + i] = randnum(1, 40);
    }
  }
}

void init_matrix_double(double *m, double value, int row, int N) {
  int i, j;
  if (row == 0) {
    for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
        m[i * N + j] = 0;
        if (value != 0)
          m[i * N + j] = randnum(0, 10);
      }
    }
  } else {
    for (i = 0; i < N; i++) {
      for (j = 0; j < N; j++) {
        m[j * N + i] = 0;
        if (value != 0)
          m[j * N + i] = randnum(0, 10);
      }
    }
  }
}

void print_matrix_int(int *m, int N) {
  for (int i = 0; i < N; i++) {
    printf("[");
    for (int j = 0; j < N; j++) {
      printf("%5d,", m[j * N + i]);
    }
    puts("]");
  }
}

void print_matrix_double(double *m, int row, int N) {
  if (row == 0) {
    for (int i = 0; i < N; i++) {
      printf("[");
      for (int j = 0; j < N; j++) {
        printf("%8.2f,", m[i * N + j]);
      }
      puts("]");
    }
  } else {
    for (int i = 0; i < N; i++) {
      printf("[");
      for (int j = 0; j < N; j++) {
        printf("%8.2f,", m[j * N + i]);
      }
      puts("]");
    }
  }
}

void print_all(double *A, double *B, double *C, double *R, double *temp, int *D,
               double maxA, double minA, double promA, double maxB, double minB,
               double promB, double cociente, int N) {
  puts("Matrix A");
  print_matrix_double(A, 0, N);
  puts("Matrix B");
  print_matrix_double(B, 1, N);
  puts("Matrix C");
  print_matrix_double(C, 0, N);
  puts("Matrix D");
  print_matrix_int(D, N);
  puts("Matrix temp (resultado entre CxD)");
  print_matrix_double(temp, 0, N);
  printf("max_min_prom_A (maxA,minA,promA)  \n[");
  printf(" %.5f %.5f %.5f", maxA, minA, promA);
  printf("]\n");
  printf("max_min_prom_B (maxB,minB,promB)  \n[");
  printf(" %.5f %.5f %.5f", maxB, minB, promB);
  printf("]\n");
  printf("cociente ([maxA*maxB-minA*minB]/[promA*promB]): %f\n", cociente);
  puts("Matrix R");
  print_matrix_double(R, 0, N);
}
