#ifndef MATRIX_OPS_H
#define MATRIX_OPS_H
/**
** Maximo, minimo y promedio de una matriz NxN
** matrix: matriz de NxN, con tipo de dato double
** res: arreglo de 3 elementos, con tipo de dato double
** N: dimension de la matriz
** res[0] maxMatrix, res[1] minMatrix, res[2] promMatrix
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void max_min_prom(double *matrix, double *res, int N, int first, int last);

/**
** Multiplicacion de matrices en bloques double*double
** a: matriz A de NxN ordenada por filas, con tipo de dato double
** b: matriz B de NxN ordenada por columnas, con tipo de dato double
** c: matriz C de NxN ordenada por filas, con tipo de dato double
** N: dimension de las matrices
** BS: dimension de los bloques
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void matmulblks_double(double *a, double *b, double *c, int N, int BS,
                       int first, int last);

// la multiplicacion de los bloques en si, ver matmulblks_double
void blkmul_double(double *ablk, double *bblk, double *cblk, int N, int BS);

/**
** Multiplicacion de matrices en bloques double*int
** a: matriz A de NxN ordena por filas, con tipo de dato double
** b: matriz B de NxN ordenada por columnas, con tipo de dato int
** c: matriz C de NxN ordenada por filas, con tipo de dato double
** N: dimension de las matrices
** BS: dimension de los bloques
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void matmulblks_double_int(double *a, int *b, double *c, int N, int BS,
                           int first, int last);

// la multiplicacion de los bloques en si, ver matmulblks_double_int
void blkmul_double_int(double *ablk, int *bblk, double *cblk, int N, int BS);

/**
** Multiplicacion de una matriz por un escalar
** R: matriz de NxN, con tipo de dato double
** cociente: escalar con tipo de dato double
** N: dimension de la matriz
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void matmul_scalar(double *R, double cociente, int N, int first, int last);

/**
** Añade la matriz b a la matriz a
** a: matriz A de NxN, con tipo de dato double
** b: matriz B de NxN, con tipo de dato double
** N: dimension de las matrices
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void matadd_double(double *a, double *b, int N, int first, int last);

/**
** Calcula la potencia de 2 a cada valor de la matriz
** N: dimension de la matriz
** potencias: arreglo de 2 elementos, con tipo de dato int
** ej: potencias[1] contiene la potencia de 2 del numero 1
** first: primera fila a multiplicar
** last: ultima fila a multiplicar
**/
void Pot2(int *d, int N, int *potencias, int first, int last);

#endif
