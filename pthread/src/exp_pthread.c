#include "matrix_ops.h"
#include "utils.h"
#include <float.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int N, T, BS;
double *A, *B, *C, *R, *temp, *res, cociente, init_tick, final_tick, maxA, minA,
    promA, maxB, minB, promB;
int *D, *potencias, offset, pasaron = 0;
pthread_barrier_t barrier;
pthread_mutex_t mutexMaxMinPromA, mutexMaxMinPromB;

void calcular_potencias(int *potencias, int N) {
  int register i;
  for (i = 1; i < N; i++) {
    potencias[i] = i * i;
  }
}

void *worker(void *args) {
  int register id, first, last, offset;
  id = *((int *)args);
  first = id * (N / T);
  last = first + (N / T);
  offset = id * 6;
  if (id == 0)
    calcular_potencias(potencias, 41);

  // res[0]=maxA, res[1]=minA, res[2]=promA
  max_min_prom(A, &res[offset], N, first, last);
  pthread_mutex_lock(&mutexMaxMinPromA);
  if (res[offset] > maxA) {
    maxA = res[offset];
  }
  if (res[offset + 1] < minA) {
    minA = res[offset + 1];
  }
  promA += res[offset + 2];

  pthread_mutex_unlock(&mutexMaxMinPromA);

  // res[3]=maxB, res[4]=minB, res[5]=promB
  max_min_prom(B, &res[offset + 3], N, first, last);

  pthread_mutex_lock(&mutexMaxMinPromB);
  if (res[offset + 3] > maxB) {
    maxB = res[offset + 3];
  }
  if (res[offset + 4] < minB) {
    minB = res[offset + 4];
  }

  promB += res[offset + 5];

  if (++pasaron == T) { // si todos los hilos pasaron
    cociente = (promA * promB != 0)
                   ? (maxA * maxB - minA * minB) / (promA * promB)
                   : 0;
  }
  pthread_mutex_unlock(&mutexMaxMinPromB);

  matmulblks_double(A, B, R, N, BS, first, last); // AxB
  pthread_barrier_wait(&barrier); // espera a que compute AxB en todos los hilos junto con el cociente
  matmul_scalar(R, cociente, N, first, last); // (AxB)*cociente
  Pot2(D, N, potencias, first, last); // D^2
  pthread_barrier_wait(&barrier); // espera a que compute D^2 en todos los hilos
  matmulblks_double_int(C, D, temp, N, BS, first, last); // CxD^2
  matadd_double(R, temp, N, first, last); // (AxB)*cociente + CxD^2
  pthread_exit(0);
}

int main(int argc, char *argv[]) {

  // ∨∨∨∨ checkeo de parametros ∨∨∨∨
  if ((argc != 4) || ((N = atoi(argv[1])) <= 0) || ((T = atoi(argv[2])) <= 0) ||
      ((BS = atoi(argv[3])) <= 0) || (N % (T * BS) != 0)) {
    printf("Error en los parámetros. Usage: %s N T BS\n", argv[0]);
    printf("N%%(T*BS) debe ser 0\n");
    exit(1);
  }
#ifdef CANT /* si se compila con -D CANT=NUMERO */
  double promedio = 0;
  for (size_t i = 0; i < CANT; i++) {
#endif /* ifdef CANT */

    // ∨∨∨∨ fase inicializacion ∨∨∨∨
    res = alloc_double(T * 6);
    A = alloc_double(N * N);
    B = alloc_double(N * N);
    C = alloc_double(N * N);
    R = alloc_double(N * N);
    temp = alloc_double(N * N);
    D = (int *)malloc(sizeof(int) * N * N);
    potencias = (int *)malloc(sizeof(int) * 41);
    for (int i = 0; i < T; i++) {
      res[i * 6] = DBL_MIN;     // maximo de A en el thread i
      res[i * 6 + 1] = DBL_MAX; // minimo de A en el thread i
      res[i * 6 + 2] = 0;       // promedio de A en el thread i
      res[i * 6 + 3] = DBL_MIN; // maximo de B en el thread i
      res[i * 6 + 4] = DBL_MAX; // minimo de B en el thread i
      res[i * 6 + 5] = 0;       // promedio de B en el thread i
    }
    maxA = DBL_MIN;
    minA = DBL_MAX;
    promB = 0;
    maxB = DBL_MIN;
    minB = DBL_MAX;
    promB = 0;
    init_matrix_double(A, 5, 0, N);
    init_matrix_double(B, 5, 1, N);
    init_matrix_double(C, 5, 0, N);
    init_matrix_double(temp, 0, 0, N);
    init_vector(potencias, 41);
    init_matrix_int(D, 2, N);
    pthread_barrier_init(&barrier, NULL, T);
    pthread_mutex_init(&mutexMaxMinPromA, NULL);
    pthread_mutex_init(&mutexMaxMinPromB, NULL);
    pthread_t threads[T];
    int ids[T], i;
    // ∨∨∨∨ fase procesamiento ∨∨∨∨
    init_tick = dwalltime();
    for (i = 0; i < T; i++) {
      ids[i] = i;
      pthread_create(&threads[i], NULL, worker, &ids[i]);
    }

    for (i = 0; i < T; i++) {
      pthread_join(threads[i], NULL);
    }
    final_tick = dwalltime();
    // ∨∨∨∨ fase imprimir y liberar ∨∨∨∨
    printf("Tiempo en segundos %f \n", final_tick - init_tick);
#ifdef DEBUG
    print_all(A, B, C, R, temp, D, maxA, minA, promA, maxB, minB, promB,
              cociente, N);
#endif
    free(A);
    free(B);
    free(C);
    free(R);
    free(D);
    free(temp);
    free(res);
    free(potencias);
#ifdef CANT /* end del for */
    promedio += (final_tick - init_tick) / CANT;
  }
  printf("N: %d | T: %d | BS: %d | Promedio: %f\n", N, T, BS, promedio);
#endif /* ifdef CANT */
  return 0;
}
